package com.nextgear.sample.exceptions;

public class StudentNotFoundException extends ResourceNotFoundException
{
    public StudentNotFoundException(Long studentId)
    {
        super("Student", "id", studentId);
    }
}
