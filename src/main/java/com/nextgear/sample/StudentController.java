package com.nextgear.sample;

import com.nextgear.sample.exceptions.StudentNotFoundException;
import com.nextgear.sample.model.Student;
import com.nextgear.sample.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentController
{
    @Autowired
    StudentRepository studentRepository;

    /**
     * Get the list of all students. Exposed as GET /api/students
     *
     * @return The list of all students from the repository
     */
    @GetMapping("/students")
    public List<Student> getAllStudents()
    {
        return studentRepository.findAll();
    }

    /**
     * Create a new student. Exposed as POST /api/students
     *
     * @param student The information for the student to create
     * @return The new student record in the repository, including ID
     */
    @PostMapping("/students")
    public Student createStudent(@Valid @RequestBody Student student)
    {
        return studentRepository.save(student);
    }

    /**
     * Retrieves a student by ID. Exposed as GET /api/students/{id}
     *
     * @param studentId The ID for the student to retrieve
     * @return The student record from the repository
     * @throws StudentNotFoundException if the student could not be found
     */
    @GetMapping("/students/{id}")
    public Student getStudentById(@PathVariable(value = "id") Long studentId)
    {
        return studentRepository
                .findById(studentId)
                .orElseThrow(() -> new StudentNotFoundException(studentId));
    }

    /**
     * Update a student's data. Exposed as PUT /api/students/{id}
     *
     * @param studentId The ID for the student to update
     * @param studentDetails The new data for the student
     * @return The updated student record from the repository
     * @throws StudentNotFoundException if the student could not be found
     */
    @PutMapping("/students/{id}")
    public Student updateStudent(
            @PathVariable(value = "id") Long studentId,
            @Valid @RequestBody Student studentDetails)
    {
        Student student = getStudentById(studentId);

        student.setFirstName(studentDetails.getFirstName());
        student.setLastName(studentDetails.getLastName());
        student.setAddress(studentDetails.getAddress());
        student.setCity(studentDetails.getCity());
        student.setState(studentDetails.getState());
        student.setZip(studentDetails.getZip());

        return studentRepository.save(student);
    }

    /**
     * Delete a student by ID. Exposed as DELETE /api/students/{id}
     *
     * @param studentId The ID for the student to delete
     * @return An OK response entity
     * @throws StudentNotFoundException if the student could not be found
     */
    @DeleteMapping("/students/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable(value = "id") Long studentId)
    {
        Student student = getStudentById(studentId);

        studentRepository.delete(student);

        return ResponseEntity.ok().build();
    }}
