#Readme
##Database Setup
The setup I had to do for database in case I need to do it again

* Installing MySQL:
  * brew install mysql
* Setup a root password
  * mysql_secure_installation
* Start MySQL
  * As a service:
    * brew services start mysql
  * Just run not as a service:
    * mysql.server start
* Add user
  * mysql -uroot -p
  * GRANT ALL PRIVILEGES ON *.* TO 'ngc-user'@'localhost' IDENTIFIED BY 'ngc-password';
  * \q
* Create database:
  * mysql -ungc-user -p
  * enter password
  * CREATE DATABASE sample_db;
  * \q
  
#Running
To run the server (on Mac, use gradlew.bat on Windows)
* ./gradlew springBoot

#Testing
A PostMan collection is included:
* StudentApp.postman_collection.json
